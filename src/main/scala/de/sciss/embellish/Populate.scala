/*
 *  Populate.scala
 *  (Strip and Embellish)
 *
 *  Copyright (c) 2022-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.embellish

import de.sciss.lucre.synth.Txn
import de.sciss.nuages
import de.sciss.nuages.{Nuages, ScissProcs, WolkenpumpeMain}

object Populate {
  def apply[T <: Txn[T]](n: Nuages[T], nm: WolkenpumpeMain[T], nConfig: Nuages.Config, sConfig: ScissProcs.Config)
                        (implicit tx: T): Unit = {
    implicit val _n: Nuages[T] = n
    val dsl = nuages.DSL[T]

    IMU_Reception (nm, dsl, sConfig, nConfig)
  }
}