/*
 *  Main.scala
 *  (Strip and Embellish)
 *
 *  Copyright (c) 2022-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.embellish

import de.sciss.desktop.Util
import de.sciss.embellish.SessionCut.TimelineName
import de.sciss.equal.Implicits._
import de.sciss.file._
import de.sciss.lucre.Txn.peer
import de.sciss.lucre.store.BerkeleyDB
import de.sciss.lucre.synth.{InMemory, Server, Synth, Txn}
import de.sciss.lucre.{Copy, Cursor, Folder, Source, Txn => LTxn}
import de.sciss.{nuages, osc}
import de.sciss.nuages.Nuages.Surface
import de.sciss.nuages.{NamedBusConfig, Nuages, ScissProcs, Wolkenpumpe, WolkenpumpeMain}
import de.sciss.numbers.Implicits._
import de.sciss.proc.Implicits.FolderOps
import de.sciss.proc.{AuralSystem, Durable, FScape, TimeRef, Timeline, Universe, Workspace}
import de.sciss.submin.Submin
import de.sciss.synth.{SynthGraph, ThirdPartyUGens, addAfter}
import jpen.event.{PenAdapter, PenManagerListener}
import jpen.owner.multiAwt.AwtPenToolkit
import jpen.{PLevel, PLevelEvent, PenDevice, PenProvider}

import java.net.{InetAddress, InetSocketAddress}
import java.text.SimpleDateFormat
import java.util.{Locale, TimerTask}
import scala.collection.immutable.{IndexedSeq => Vec}
import scala.concurrent.stm.Ref
import scala.swing.event.ButtonClicked
import scala.swing.{Button, Swing}
import scala.util.control.NonFatal

object Main {
  def mkDatabase(parent: File): File = {
    require (parent.isDirectory, s"Not a directory: $parent")
    val recFormat = new SimpleDateFormat("'session_'yyMMdd'_'HHmmss'.mllt'", Locale.US)
    parent / recFormat.format(new java.util.Date)
  }

  def version     : String = buildInfString("version")
  def license     : String = buildInfString("license")
  def homepage    : String = buildInfString("homepage")
  def builtAt     : String = buildInfString("builtAtString")
  def fullVersion : String = s"v$version, built $builtAt"

  private def buildInfString(key: String): String = try {
    val clazz = Class.forName("de.sciss.anemone.BuildInfo")
    val m     = clazz.getMethod(key)
    m.invoke(null).toString
  } catch {
    case NonFatal(_) => "?"
  }

  final case class Config(
                           masterChannels    : Range,
                           masterGroups      : Vec[NamedBusConfig] = Vector.empty,
                           soloChannels      : Range,
                           micInputs         : Vec[NamedBusConfig],
                           lineInputs        : Vec[NamedBusConfig],
                           lineOutputs       : Vec[NamedBusConfig],
                           genNumChannels    : Int                 = 0,
                           device            : Option[String]      = None,
                           database          : Option[File]        = None,
                           timeline          : Boolean             = true,
                           tablet            : Boolean             = true,
                           inputTimeline     : Option[File]        = None,
                           dimmer            : Option[(String, Int)] = None,
                         )

  val NoSolo: Range = 0 until 0

  lazy val sessionDir: File = userHome / "Documents" / "projects" / "StripAndEmbellish" / "sessions"

  lazy val xCoAx: Config = Config(
    masterChannels    = 0 until 2,
    soloChannels      = 0 until 0,
    genNumChannels    = 2,
    micInputs         = Vector(
      NamedBusConfig("m-dpa"  , 0 until 2),
      NamedBusConfig("m-pozzi", 2 until 4),
    ),
    lineInputs      = Vector.empty,
    lineOutputs     = Vector.empty,
    device        = Some("Wolkenpumpe"),
    database      = Some(mkDatabase(sessionDir)),
    inputTimeline = Some(sessionDir / "se-dispo-weimar.mllt"),
    dimmer        = Some(("192.168.77.100", 57120)),
    timeline  = true
  )

  lazy val Cube: Config = Config(
    masterChannels = 2 until 4,
    soloChannels = 0 until 0,
    genNumChannels = 2,
    micInputs = Vector(
      NamedBusConfig("m-room" , 0 until 2),
      NamedBusConfig("m-pozzi", 2 until 4),
    ),
    lineInputs = Vector.empty,
    lineOutputs = Vector(
      NamedBusConfig("pozzi", 4 until 6),
    ),
    device = Some("Wolkenpumpe"),
    database = Some(mkDatabase(userHome / "Documents" / "projects" / "StripAndEmbellish" / "sessions")),
    timeline = true
  )

  lazy val Kunstverein: Config = Config(
    masterChannels = 0 until 2,
    soloChannels = 0 until 0,
    genNumChannels = 2,
    micInputs = Vector(
      NamedBusConfig("m-pozzi", 0 until 2),
    ),
    lineInputs = Vector.empty,
    lineOutputs = Vector.empty,
    device = Some("Wolkenpumpe"),
    database = Some(mkDatabase(sessionDir)),
    inputTimeline = None, // Some(sessionDir / "se-dispo-weimar.mllt"),
    dimmer = None,
    timeline = true
  )

  lazy val Advent: Config = Config(
    masterChannels = 2 until 4,
    soloChannels = 0 until 0,
    genNumChannels = 2,
    micInputs = Vector(
      NamedBusConfig("m-room", 0 until 2),
      NamedBusConfig("m-pozzi", 2 until 4),
    ),
    lineInputs = Vector.empty,
    lineOutputs = Vector(
      NamedBusConfig("pozzi", 0 until 2),
    ),
    device = Some("Wolkenpumpe"),
    database = Some(mkDatabase(userHome / "Documents" / "projects" / "StripAndEmbellish" / "sessions")),
    timeline = true
  )

  private val config: Config = Advent

  def mkSurface[T <: Txn[T]](config: Config, tlOpt: Option[Source[T, Timeline.Modifiable[T]]])
                            (implicit tx: T): Surface[T] =
    if (config.timeline) {
      val tl = tlOpt.fold(Timeline[T]())(_.apply())
      Surface.Timeline(tl)
    } else {
      val f = Folder[T]()
      Surface.Folder(f)
    }

  private def createWorkspace(folder: File): Workspace.Durable = {
    val config          = BerkeleyDB.Config()
    config.allowCreate  = true
    val ds              = BerkeleyDB.factory(folder, config)
    // config.lockTimeout  = Duration(Prefs.dbLockTimeout.getOrElse(Prefs.defaultDbLockTimeout), TimeUnit.MILLISECONDS)
    val meta = Map(
      (Workspace.KeySoundProcessesVersion, de.sciss.proc.BuildInfo.version),
    )

    Workspace.Durable.empty(folder.toURI, ds, meta = meta)
  }

  def mkTimelineInput[Out <: Txn[Out]](config: Config)
                                      (implicit cursorOut: Cursor[Out]): Option[Source[Out, Timeline.Modifiable[Out]]] =
    config.inputTimeline.map { fIn =>
      type In   = Durable.Txn
      val dsIn  = BerkeleyDB.factory(fIn, createIfNecessary = false)
      val wsIn  = Workspace.Durable.read(fIn.toURI, dsIn)
      implicit val cursorIn: Cursor[In] = wsIn.cursor
      LTxn.copy[In, Out, Source[Out, Timeline.Modifiable[Out]]] { (txIn0: In, txOut0: Out) =>
        implicit val txIn: In = txIn0
        implicit val txOut: Out = txOut0
        val copy  = Copy[In, Out]()
        val tlIn0 = wsIn.root.$[Timeline](TimelineName)
          .getOrElse(sys.error(s"No timeline named '$TimelineName' found in $fIn"))
        val tlIn  = tlIn0.modifiableOption.getOrElse(sys.error("Timeline not modifiable ??"))
        val tlOut = copy(tlIn)
        copy.finish()
        txOut.newHandle(tlOut)
      }
    }

  def main(args: Array[String]): Unit = {
    println(s"Strip and Embellish $fullVersion")
    nuages.showLog = false
    // de.sciss.nuages. DSL.useScanFixed = true
    // defer(WebLookAndFeel.install())
    Submin.install(true)
    Wolkenpumpe .init()
    FScape      .init()
    ThirdPartyUGens.init()

    config.database match {
      case Some(f) =>
        type S = Durable
        type T = Durable.Txn
        val ws = createWorkspace(f)
        implicit val system: S = Durable(BerkeleyDB.factory(f))
        val anemone = new Main[T](config)
        val tlCopyOpt = mkTimelineInput[T](config)
        val nuagesH = system.step { implicit tx =>
          val n = Nuages[T](mkSurface[T](config, tlCopyOpt))
          ws.root.addLast(n)
          tx.newHandle(n)
        }
        anemone.run(nuagesH)

      case None =>
        type S = InMemory
        type T = InMemory.Txn
        implicit val system: S = InMemory()
        val anemone = new Main[T](config)
        val tlCopyOpt = mkTimelineInput[T](config)
        val nuagesH = system.step { implicit tx =>
          val n = Nuages[T](mkSurface(config, tlCopyOpt))
          tx.newHandle(n)
        }
        anemone.run(nuagesH)
    }
  }
}
class Main[T <: Txn[T]](config: Main.Config) extends WolkenpumpeMain[T] {

  override protected def configure(sCfg: ScissProcs.ConfigBuilder, nCfg: Nuages.ConfigBuilder,
                                   aCfg: Server.ConfigBuilder): Unit = {
    super.configure(sCfg, nCfg, aCfg)
    sCfg.genNumChannels  = config.genNumChannels
    // println(s"generatorChannels ${sCfg.generatorChannels}")
    nCfg.micInputs          = config.micInputs
    nCfg.lineInputs         = config.lineInputs
    nCfg.lineOutputs        = config.lineOutputs
    sCfg.mainGroups         = config.masterGroups
    // sCfg.highPass           = 100
    sCfg.audioFilesFolder   = Some(userHome / "Music" / "tapes")
    sCfg.plugins            = true
    sCfg.recDir             = userHome / "Documents" / "projects" / "StripAndEmbellish" / "audio_work" / "nuages"

    require (sCfg.recDir.isDirectory)

    // println(s"master max = ${Turbulence.ChannelIndices.max}")
    nCfg.mainChannels       = Some(config.masterChannels)
    nCfg.soloChannels       = if (config.soloChannels.nonEmpty) Some(config.soloChannels) else None
    nCfg.recordPath         = Some((userHome / "Music" / "rec").path) // XXX has no effect?

    aCfg.wireBuffers        = 512 // 1024
    aCfg.audioBuffers       = 4096
    // aCfg.blockSize          = 128
    if (config.device.isDefined) aCfg.deviceName = config.device
  }

  override protected def registerProcesses(nuages: Nuages[T], nCfg: Nuages.Config, sCfg: ScissProcs.Config)
                                          (implicit tx: T, universe: Universe[T]): Unit = {
    super.registerProcesses(nuages, nCfg, sCfg)
    Populate(nuages, this, nCfg, sCfg)
  }

  def initTablet(): Unit = {
    val penManager = AwtPenToolkit.getPenManager
    penManager.addListener(new PenManagerListener {
      def penDeviceAdded(c: PenProvider.Constructor, d: PenDevice): Unit = {
        println(s"penDeviceAdded($c, $d)")
      }

      def penDeviceRemoved(c: PenProvider.Constructor, d: PenDevice): Unit = ()
    })

    val awtComp = view.panel.display
    AwtPenToolkit.addPenListener(awtComp, new PenAdapter {
      //      override def penButtonEvent(ev: PButtonEvent): Unit =
      //        println(s"penButtonEvent($ev)")

      @volatile
      private[this] var lastLevel = 0f
      private[this] val panel     = view.panel
      // we take the maximum of these readings, so we don't lose the level when releasing the pen
      private[this] val levelMax  = new Array[Float](6)
      private[this] var levelMaxI = 0

      override def penLevelEvent(ev: PLevelEvent): Unit = {
        //        println(s"penLevelEvent($ev)")
        val levels = ev.levels
        //        println(s"level: ${levels.mkString("[", ", ", "]")}")
        var i = 0
        while (i < levels.length) {
          val lvl = levels(i)

          // JPen 2.4 -- Tilt information is broken: https://github.com/nicarran/jpen/issues/12
          if (lvl.getType === PLevel.Type.PRESSURE /* TILT_Y */) {
            val raw: Float = lvl.value
            val arr = levelMax
            arr(levelMaxI) = raw
            levelMaxI = (levelMaxI + 1) % arr.length
            var j = 1
            var max = arr(0)
            while (j < arr.length) {
              val k = arr(j)
              if (k > max) max = k
              j += 1
            }
            val lvlClip = math.min(1.0f, math.max(0f, max - 0.1f) / 0.9f)
            // println(s"$raw | $max | $lvlClip | $lastLevel | ${panel.acceptGlideTime}")
            if (lvlClip != lastLevel) {
              lastLevel = lvlClip
              if (panel.acceptGlideTime) Swing.onEDT {
                if (panel.glideTimeSource !== "key") {
                  panel.glideTime       = lastLevel
                  panel.glideTimeSource = "tablet"
                }
              }
            }
            i = levels.length
          } else {
            i += 1
          }
        }
      }

      //      override def penKindEvent(ev: PKindEvent): Unit = {
      //        // println(s"penKindEvent($ev)")
      //        println(s"kind: ${ev.kind}")
      //      }

      //      override def penScrollEvent(ev: PScrollEvent): Unit =
      //        println(s"penScrollEvent($ev)")

      //      override def penTock(availableMillis: Long): Unit =
      //        println(s"penTock($availableMillis)")
    })
  }

  private val synEmbellishRef = Ref[Option[Synth]](None)


  override def run(nuagesH: Source[T, Nuages[T]])(implicit cursor: Cursor[T]): Unit = {
    super.run(nuagesH)

    val hasDimmer = config.dimmer.isDefined
    val funLight: Double => Unit = config.dimmer.fold((_: Double) => ()) { case (tgtHost, tgtPort) =>
      val cfgT = osc.UDP.Config()
      cfgT.localIsLoopback = false
      cfgT.localAddress = InetAddress.getByName("192.168.77.78")
      val t = osc.UDP.Transmitter(cfgT)
      t.connect()
      val tgt = new InetSocketAddress(tgtHost, tgtPort)
      (d: Double) => {
        val lvl = d.clip(0.0, 1.0).linExp(0.0, 1.0, 1, 128).toInt - 1
        t.send(osc.Message("/ramp", lvl), tgt)
      }
    }

    val dimmerTimer = new java.util.Timer("fade-dimmer")

    val durSec = 10.0

    Swing.onEDT {
      val ggEmbellish = new Button("Embellish")
      Util.fixWidth(ggEmbellish)

      ggEmbellish.reactions += {
        case ButtonClicked(_) =>
          ggEmbellish.text = "Fade Out"

          val tStart = System.currentTimeMillis()
          val timerTask = new TimerTask {
            override def run(): Unit = {
              val tNow = System.currentTimeMillis()
              val dt = (tNow - tStart) * 0.001
              val dNorm = dt.clip(0.0, durSec).linLin(0.0, durSec, 0.0, 1.0)
              funLight(dNorm)
            }
          }

          if (hasDimmer) {
            dimmerTimer.scheduleAtFixedRate(timerTask, 39, 39)
          }

          cursor.step { implicit tx =>
            synEmbellishRef().foreach(_.set("gate" -> 0))
            val sch = view.panel.universe.scheduler
            sch.schedule(sch.time + (TimeRef.SampleRate * (durSec + 0.5)).toLong) { implicit tx =>
              val t = view.panel.transport
              t.stop()
              t.seek(t.position/2)
              t.play()
              synEmbellishRef().foreach(_.set("gate" -> 1))
              tx.afterCommit {
                ggEmbellish.text = "Fade In"
                timerTask.cancel()
                funLight(0.0) // back to black
              }
              sch.schedule(sch.time + (TimeRef.SampleRate * 10.5).toLong) { implicit tx =>
                tx.afterCommit {
                  ggEmbellish.text = "Done"
                }
              }
            }
          }
      }
      view.addSouthComponent(ggEmbellish)
    }

    if (config.tablet) cursor.step { implicit tx =>
      auralSystem.reactNow { implicit tx => {
        case AuralSystem.Running(server) =>
          Swing.onEDT {
            initTablet()
          }

          val gEmbellish = SynthGraph {
            import de.sciss.synth.ugen._
            import de.sciss.synth.Import._
            import de.sciss.synth.Ops.stringToControl
            val in  = In.ar(0, 2)
            val env = Env.asr(attack = "atk".kr(5f), release = "rls".kr(10f))
            val eg  = EnvGen.kr(env, gate = "gate".kr(1))
            val sig = in * eg
            ReplaceOut.ar(0, sig)
          }
          val synEmbellish = Synth.play(gEmbellish, Some("embellish"))(server.defaultGroup, Seq("rls" -> durSec), addAction = addAfter)
          synEmbellishRef() = Some(synEmbellish)

        case _ => ()
      }}
    }
  }
}
