/*
 *  SessionCut.scala
 *  (Strip and Embellish)
 *
 *  Copyright (c) 2022-2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.embellish

import de.sciss.file._
import de.sciss.lucre.{Copy, Cursor, Obj, Source, SpanLikeObj, Txn}
import de.sciss.lucre.store.BerkeleyDB
import de.sciss.lucre.synth.Executor
import de.sciss.nuages.{Nuages, Wolkenpumpe}
import de.sciss.proc.Implicits.ObjOps
import de.sciss.proc.{Durable, FScape, Grapheme, Proc, SoundProcesses, TimeRef, Timeline, Workspace}
import de.sciss.span.Span
import de.sciss.synth.ThirdPartyUGens
import org.rogach.scallop.{ScallopConf, ScallopOption => Opt}

import scala.collection.mutable

object SessionCut {
  case class Config(
                   fileIn : File    = file("input.mllt"),
                   fileOut: File    = file("output.mllt"),
                   offset : Double  = 60.0 * 14,
                   )

  final val TimelineName = "embellish-cut"

  def main(args: Array[String]): Unit = {
    object p extends ScallopConf(args) {
      import org.rogach.scallop._

      printedName = "SessionCut"
      private val default = Config()

      val input: Opt[File] = opt(required = true,
        descr = "Input Mellite workspace",
      )
      val output: Opt[File] = opt(required = true,
        descr = "Output Mellite workspace",
      )
      val offset: Opt[Double] = opt(default = Some(default.offset),
        descr = f"Timeline cutting offset in seconds (default: ${default.offset}%1.2f).",
      )
//      val verbose: Opt[Boolean] = toggle(default = Some(default.verbose),
//        descrYes = "Turn verbose on",
//      )

      verify()

      val config: Config = Config(
        fileIn  = input(),
        fileOut = output(),
        offset  = offset(),
//        verbose = verbose(),
      )
    }

    implicit val c: Config = p.config
    run()
  }

  def makeCopy[In <: Txn[In], Out <: Txn[Out]](wsIn: Workspace[In], wsOut: Workspace[Out],
                                               tlInS: Source[In, Timeline[In]], tlOutS: Source[Out, Timeline.Modifiable[Out]])
                                              (implicit config: Config): Unit = {
    Txn.copy[In, Out, Unit] { (txIn0: In, txOut0: Out) =>
      implicit val txIn : In  = txIn0
      implicit val txOut: Out = txOut0
      val copy    = Copy[In, Out]()
      val spanOut = Span.From(0L)
//      val spanOut = Span(0L, (TimeRef.SampleRate * 60 * 20).toLong) // XXX TODO test finite span

      val offsetFr = (config.offset * TimeRef.SampleRate).toLong
      val tlIn  = tlInS()
      val tlOut = tlOutS()
      val it    = tlIn.intersect(offsetFr).toSeq
      val objSeq: Seq[(Long, Obj[In])] = it.flatMap { case (spanLike, vec) =>
        val regOff = spanLike.startOption.getOrElse(0L)
        vec.map(entry => (regOff, entry.value))
      }
      println(s"Found ${it.size} regions with ${objSeq.size} objects")

      var procSq = mutable.Seq.empty[(Proc[In], Proc[Out])]

      objSeq.foreach { case (regOff, obj) =>
        val deltaFr = offsetFr - regOff
        assert(deltaFr >= 0L)
        println(f"$obj - ${obj.name} at ${deltaFr / TimeRef.SampleRate}%1.2fs")

        val objC  = copy.copyPlain(obj)

        (obj, objC) match {
          case (pIn: Proc[In], pOut: Proc[Out]) =>
            procSq +:= ((pIn, pOut))
//            println(s"  outputs source: ${pIn.outputs.keys}")
//            println(s"  outputs target: ${pOut.outputs.keys}")

          case _ =>
        }

        val attrC = objC.attr

        // we'll find in the attribute map:
        // - grapheme : replace by momentary value
        // - timeline : replace by recursive 'cut'
        // - other    : copy verbatim (param specs, expr variables)
        txIn.attrMapOption(obj).foreach { attr =>
          attr.iterator.foreach { case (key, value) =>
            println(s"  $key -> $value")
            val valueOptC = value match {
              case gr: Grapheme[In] =>
                gr.at(deltaFr).map { grE =>
                  val grV = grE.value
                  println(s"    = $grV")
                  copy(grV)
                }

              case tl: Timeline[In] =>
                val sq = tl.intersect(deltaFr).toSeq
                val tlC = Timeline[Out]()
                sq.foreach { case (span, vec) =>
                  println(s"    = $span -> Vec(")
//                  val tlSpanC     = span.shift(-deltaFr).intersect(Span.From(0L))
                  val tlSpanC     = Span.From(0L)
                  val tlSpanObjC  = SpanLikeObj.newVar[Out](tlSpanC)  // XXX TODO or use constants?
                  vec.foreach { tlE =>
                    val tlV = tlE.value
                    println(s"        $tlV,")
                    tlC.add(tlSpanObjC, copy(tlV))
                  }
                  println("      )")
                }
                Some(tlC)

//              case pIn: Proc[In] =>
//                val pOut = copy(p)
//                println(s"outputs source: ${pIn.outputs.keys}")
//                println(s"outputs target: ${pOut.outputs.keys}")
//                Some(copy(value))

              case _ => Some(copy(value))
            }
            valueOptC.foreach { valueC =>
              attrC.put(key, valueC)
            }
          }
        }

        val spanC = SpanLikeObj.newVar[Out](spanOut)  // XXX TODO or use constants?
        tlOut.add(spanC, objC)
      }
      copy.finish()

      procSq.foreach { case (pIn, pOut) =>
        println(s"  ${pIn .name} outputs source: ${pIn.outputs.keys}")
        println(s"  ${pOut.name} outputs target: ${pOut.outputs.keys}")
      }

      ()
    } (wsIn.cursor, wsOut.cursor)
  }

  def run()(implicit config: Config): Unit = {
    Wolkenpumpe     .init()
    FScape          .init()
    ThirdPartyUGens .init()

    type T  = Durable.Txn

    val dsIn  = BerkeleyDB.factory(config.fileIn, createIfNecessary = false)
    val wsIn  = Workspace.Durable.read(config.fileIn.toURI, dsIn)
//    implicit val cursorIn: Cursor[T] = wsIn.cursor

    SoundProcesses.step[T]("open input session") { implicit tx =>
      val rootIn  = wsIn.root
      val tlE     = rootIn.headOption match {
        case Some(n: Nuages[T]) =>
          n.surface match {
            case Nuages.Surface.Timeline(tl) => Right(tl)
            case _ => Left("Not a timeline session")
          }

        case _ => Left("Session does not begin with a Nuages object")
      }

      tlE match {
        case Right(tlIn) =>
          val tlInS = tx.newHandle(tlIn)
          tx.afterCommit {
            val dscOut = BerkeleyDB.Config()
            dscOut.allowCreate = true
            val dsOut = BerkeleyDB.factory(config.fileOut, dscOut)
            val wsOut = Workspace.Durable.empty(config.fileOut.toURI, dsOut)
            val tlOutS = wsOut.cursor.step { implicit tx =>
              val rootOut = wsOut.root
              val tlOut = Timeline[T]()
              tlOut.name = TimelineName
              rootOut.addLast(tlOut)
              tx.newHandle(tlOut)
            }

            makeCopy[T, T](wsIn, wsOut, tlInS, tlOutS)
            wsIn  .close()
            wsOut .close()
            sys.exit(0)
          }

        case Left(err) =>
          tx.afterCommit {
            Console.err.println(err)
            wsIn.close()
            sys.exit(1)
          }
      }
    } (wsIn.cursor)

//    new Thread {
//      override def run(): Unit = synchronized(wait())
//      start()
//    }
  }
}
