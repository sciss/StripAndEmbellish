# Strip and Embellish

[![Build Status](https://github.com/Sciss/StripAndEmbellish/workflows/Scala%20CI/badge.svg?branch=main)](https://github.com/Sciss/StripAndEmbellish/actions?query=workflow%3A%22Scala+CI%22)

This project contains my personal set-up for a live improvisation project.
It is an extension of [Wolkenpumpe](https://codeberg.org/sciss/Wolkenpumpe).

All code here
is (C)opyright 2022-2023 by Hanns Holger Rutz. All rights reserved. This project is released under the
[GNU General Public License](https://codeberg.org/sciss/StripAndEmbellish/raw/main/LICENSE) v3+ and comes with absolutely no warranties.
To contact the author, send an e-mail to `contact at sciss.de`.

## building

Builds with sbt against Scala 2.13. Use `sbt run`, or `sbt assembly` to create a self-contained jar (you may need the
latter if you want to add the tablet library to the system's class path).

## running

To use the Wacom controls, add environment variable `LD_LIBRARY_PATH=lib` to the JVM run
(currently only Linux native library included).

## sensors

In project ['Tinker'](https://codeberg.org/sciss/Tinker) (`cd ../Tinker/`):

Daniele:

    ./scripts/imu_osc.sh --uid Zpw 2146 --target-host 192.168.77.100 --target-port 57120 --source-host 192.168.77.78 

Hanns Holger:

    ./scripts/imu_osc.sh --uid Zpw 2146 --target-port 7771 # --target-host 192.168.77.78

## audio wiring

Babyface HHR:

- DPA is Mic in 1-2
- Main out 1-2 goes to Daniele
- Headphones out goes via splitter to mixer
- Daniele's signal coming in at in 3-4

## to-do list

packaging:

- [ ] Stromverteiler 3fach oder mehr
- [ ] Ethernetkabel
- [ ] Mini-Mixer
- [ ] MIDI-Dimmer
- [ ] 2x Lampen
- [ ] Fostex FR-2LE

code:

- [ ] "re-zero" timer, so that we can use an offset without restarting
- [X] euler -> a-skew -> only left channel?
